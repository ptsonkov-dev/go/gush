package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strings"
	"sync"

	"github.com/fatih/color"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"
	"gopkg.in/yaml.v2"
)

// read and parse command line parameters and flags
func readParams() (map[string]string, map[string]bool) {

	// Define returned maps
	paramsString := make(map[string]string)
	paramsBoolean := make(map[string]bool)

	helpString := "Command syntax: \n # gush [-n|-g] <nodeList|groupList> '<commandInSingleQuotes>' \n\nUse -h for more parameter info."

	// String flags
	nodeFlag := "n"
	groupFlag := "g"
	pNode := flag.String(nodeFlag, "", "Target node(s) coma (,) delimited. Type 'all' to execute on all nodes")
	pGroup := flag.String(groupFlag, "", "Target group(s), coma (,) delimited. Node list overrides group list")

	// Boolean flags
	listFlag := "l"
	seqFlag := "seq"
	pList := flag.Bool(listFlag, false, "List all nodes, groups and group members")
	pSequence := flag.Bool(seqFlag, false, "Execute command in sequence (slower) or in parallel (faster) to node list. Default 'parallel'")

	flag.Parse()

	if *pList == true {

		paramsString["action"] = "l"

		return paramsString, paramsBoolean
	}

	// Check target - list or group
	if *pNode == *pGroup {
		fmt.Println(helpString)
		os.Exit(0)
	} else if *pNode != "" && *pGroup != "" || *pNode != "" && *pGroup == "" {
		paramsString["action"] = nodeFlag
		paramsString["target"] = *pNode
	} else if *pNode == "" && *pGroup != "" {
		paramsString["action"] = groupFlag
		paramsString["target"] = *pGroup
	}

	// Check command
	if len(flag.Args()) == 0 {
		fmt.Println(helpString)
		os.Exit(0)
	} else {
		paramsString["command"] = flag.Args()[0]
	}

	// Append boolean flags
	paramsBoolean[seqFlag] = *pSequence

	return paramsString, paramsBoolean
}

// read and parse config file
func readConf(cmdcolor *color.Color) (string, map[string]string, map[string][]string) {

	type GrpMembers struct {
		Node string
	}

	fileName := "gush.yaml"
	validPaths := []string{os.Getenv("HOME") + "/", "/etc/", "./"}
	var yamlConfigFile string
	var noFile = true
	for _, path := range validPaths {
		yamlConfigFile = path + fileName
		if _, err := os.Stat(yamlConfigFile); err == nil {
			noFile = false
			break
		}
	}

	if noFile {
		fmt.Println("Config file '" + fileName + "' was not found at any of the valid places:")
		fmt.Println(validPaths)
		fmt.Println("Exit")
		os.Exit(1)

	}

	// Read config file
	cmdcolor.Println("Work with congig file: " + yamlConfigFile)
	data, err := ioutil.ReadFile(yamlConfigFile)
	if err != nil {
		log.Fatal(err)
	}

	// Define Mappings
	cfgFile := make(map[string]interface{})
	var cfgUser string
	cfgNodes := make(map[string]string)
	cfgGroups := make(map[string][]string)

	// Unmarshal raw config file to config map
	errUnmarshal := yaml.Unmarshal([]byte(data), &cfgFile)
	if errUnmarshal != nil {
		log.Fatalf("error: %v", errUnmarshal)
	}

	// Loop trough config map
	for confKey, confValue := range cfgFile {
		// Get value of interface
		dMap := reflect.ValueOf(confValue)
		// Check config keys
		if confKey == "user" {
			// Convert value to string
			cfgUser, _ = dMap.Interface().(string)
		} else if confKey == "nodes" {
			// Map key=>value from inteface to regular slice
			for _, nodeName := range dMap.MapKeys() {
				nodeIP := dMap.MapIndex(nodeName)
				// Convert key/value to string
				stringNodeName, _ := nodeName.Interface().(string)
				stringNodeIP, _ := nodeIP.Interface().(string)
				// Append to dictionary
				cfgNodes[stringNodeName] = stringNodeIP
			}
		} else if confKey == "groups" {
			// Map key=>value from inteface to regular slice
			for _, groupName := range dMap.MapKeys() {
				groupMembers := dMap.MapIndex(groupName)
				// Convert key/value to string
				stringGroupName, _ := groupName.Interface().(string)

				dMembers := groupMembers.Interface().([]interface{})
				var sliceGroupMembers []string
				for _, v := range dMembers {
					sliceGroupMembers = append(sliceGroupMembers, v.(string))
				}

				// Append to dictionary
				cfgGroups[stringGroupName] = sliceGroupMembers
			}
		}
	}

	return cfgUser, cfgNodes, cfgGroups
}

// Read user's private key and known_hosts file
func readKeyHostfile() (ssh.Signer, ssh.HostKeyCallback) {

	homeDir := os.Getenv("HOME")

	key, err := ioutil.ReadFile(homeDir + "/.ssh/id_rsa")
	if err != nil {
		log.Fatalf("Unable to read private key: %v", err)
	}

	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Fatalf("Unable to parse private key: %v", err)
	}

	hostKeyCallback, err := knownhosts.New(homeDir + "/.ssh/known_hosts")
	if err != nil {
		log.Fatal("Could not create hostkeycallback function: ", err)
	}

	return signer, hostKeyCallback
}

// Execute command at remote node and return result
func remoteExec(user string, address string, command string) (string, string) {

	signer, hostKeyCallback := readKeyHostfile()

	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: hostKeyCallback,
	}

	// Connect to the remote server and perform the SSH handshake.
	client, err := ssh.Dial("tcp", address, config)
	if err != nil {
		return "ERROR:", fmt.Sprintf("Unable to connect: %v \nCheck connection or config file IP:port", err)
	}
	defer client.Close()

	// Create new session
	ss, err := client.NewSession()
	if err != nil {
		log.Fatal("unable to create SSH session: ", err)
	}
	defer ss.Close()

	// Result buffers
	var stdoutBuf bytes.Buffer
	var stderrBuf bytes.Buffer
	ss.Stdout = &stdoutBuf
	ss.Stderr = &stderrBuf

	// Execute command remotely
	ss.Run(command)

	return stdoutBuf.String(), stderrBuf.String()
}

// Sequential execution of command to remote nodes
func execSequent(nodelist []string, usr string, cfgnodes map[string]string, cmd string, cmdcolor *color.Color) {
	fmt.Println("")
	for _, host := range nodelist {
		target := host + " (" + usr + "@" + cfgnodes[host] + ")"
		cmdcolor.Println(target, "=>", cmd)
		cmdOut, errOut := remoteExec(usr, cfgnodes[host], cmd)

		if cmdOut == "ERROR:" {
			color.Red(cmdOut)
			color.Red(errOut + "\n\n")
		} else {
			fmt.Println(cmdOut)
			fmt.Println(errOut)
		}
	}
}

// Parallel execution of command to remote nodes
func execParallel(nodelist []string, usr string, cfgnodes map[string]string, cmd string, cmdcolor *color.Color) {
	// Create waitgroup, channel and execute command with concurrency (goroutine)
	outchan := make(chan CommandResult)
	var wg_command sync.WaitGroup
	var wg_processing sync.WaitGroup
	for _, host := range nodelist {
		wg_command.Add(1)
		target := host + " (" + usr + "@" + cfgnodes[host] + ")"
		go func(dst, usr, ip, command string, out chan CommandResult) {
			defer wg_command.Done()
			cmdOut, errOut := remoteExec(usr, ip, cmd)
			out <- CommandResult{dst, cmdOut, errOut}
		}(target, usr, cfgnodes[host], cmd, outchan)
	}

	wg_processing.Add(1)
	go func() {
		defer wg_processing.Done()
		for o := range outchan {
			fmt.Println("")
			cmdcolor.Println(o.target, "=>", cmd)

			if o.cmdout == "ERROR:" {
				color.Red(o.cmdout)
				color.Red(o.cmderr + "\n\n")
			} else {
				fmt.Println(o.cmdout)
				fmt.Println(o.cmderr)
			}
		}
	}()

	// wait untill all goroutines to finish and close the channel
	wg_command.Wait()
	close(outchan)
	wg_processing.Wait()
}

// Check if needle is in slice (array)
func inSlice(needle string, sliceList []string) bool {

	for _, sliceElement := range sliceList {
		if sliceElement == needle {
			return true
		}
	}
	return false
}

// Check if nodes are valid (exists in config file with IP)
func checkValidNodes(nodelist []string, cfgnodes map[string]string) ([]string, []string) {
	var valid []string
	var invalid []string

	for _, i := range nodelist {
		if _, ok := cfgnodes[i]; ok {
			if !inSlice(i, valid) {
				valid = append(valid, i)
			}
		} else {
			if !inSlice(i, invalid) {
				invalid = append(invalid, i)
			}
		}
	}

	return valid, invalid
}

// Check if nodes are valid (exists in config file with IP)
func checkValidGroups(nodelist []string, cfggroups map[string][]string) ([]string, []string) {
	var valid []string
	var invalid []string

	for _, i := range nodelist {
		if _, ok := cfggroups[i]; ok {
			if !inSlice(i, valid) {
				valid = append(valid, i)
			}
		} else {
			if !inSlice(i, invalid) {
				invalid = append(invalid, i)
			}
		}
	}

	return valid, invalid
}

// ConnectionResult container
type CommandResult struct {
	target string
	cmdout string
	cmderr string
}

func main() {

	// Define colors
	// = Regular colors
	green := color.New(color.FgGreen)
	red := color.New(color.FgRed)
	// = Bold colors
	bYellow := color.New(color.FgYellow).Add(color.Bold)
	bWhite := color.New(color.FgWhite).Add(color.Bold)
	bBlue := color.New(color.FgBlue).Add(color.Bold)

	// cmdFlag, cmdTargets := readParams()
	paramString, paramBool := readParams()
	user, nodes, groups := readConf(bWhite)

	switch paramString["action"] {
	case "l":
		bYellow.Println("=== List configuration inventory (nodes and groups)")
		bBlue.Println("\nNodes: ")
		var nodeList string
		for node, _ := range nodes {
			if nodeList == "" {
				nodeList = node
			} else {
				nodeList = nodeList + "," + node
			}
		}
		fmt.Println(nodeList)

		bBlue.Println("\nGroups (and group members):")
		for group, members := range groups {
			bWhite.Println("  ", group, ": ")
			nodeList = ""
			for _, member := range members {
				if nodeList == "" {
					nodeList = member
				} else {
					nodeList = nodeList + "," + member
				}
			}
			fmt.Println("    ", nodeList)
		}
		break
	case "n":
		var execType string
		var validNodes []string
		var invalidNodes []string

		cmdExec := paramString["command"]

		if paramBool["seq"] {
			execType = "sequent, slower"
		} else {
			execType = "parallel, faster"
		}

		bYellow.Println("=== Execute command against list of nodes (" + execType + ")")

		if paramString["target"] == "all" {
			for n, _ := range nodes {
				validNodes = append(validNodes, n)
			}
		} else {
			cmdTargets := strings.Split(paramString["target"], ",")
			validNodes, invalidNodes = checkValidNodes(cmdTargets, nodes)
		}

		green.Println("target nodes: ", validNodes)
		if len(invalidNodes) != 0 {
			red.Println("not found in 'nodes' dictionary (ignoring, check config file): ", invalidNodes)
		}

		// Execute in sequence or in paralel as per command flag
		if paramBool["seq"] {
			execSequent(validNodes, user, nodes, cmdExec, bBlue)
		} else {
			execParallel(validNodes, user, nodes, cmdExec, bBlue)
		}
		break
	case "g":
		var execType string
		var validNodes []string
		var invalidNodes []string
		var validGroups []string
		var invalidGroups []string

		cmdExec := paramString["command"]

		if paramBool["seq"] {
			execType = "sequent, slower"
		} else {
			execType = "parallel, faster"
		}

		bYellow.Println("=== Execute command against group of nodes (" + execType + ")")

		// Create list with valid groups
		if paramString["target"] == "all" {
			for n, _ := range groups {
				validGroups = append(validGroups, n)
			}
		} else {
			cmdTargets := strings.Split(paramString["target"], ",")
			validGroups, invalidGroups = checkValidGroups(cmdTargets, groups)
		}

		green.Println("target groups: ", validGroups)
		if len(invalidGroups) != 0 {
			red.Println("not found in node config (ignoring, maybe error in config file?): ", invalidGroups)
		}

		// Create list with target nodes from groups
		var groupMembers []string
		for _, i := range validGroups {
			m := groups[i]
			groupMembers = append(groupMembers, m...)
		}
		validNodes, invalidNodes = checkValidNodes(groupMembers, nodes)

		green.Println("affected nodes (group members): ", validNodes)
		if len(invalidNodes) != 0 {
			red.Println("not found in 'nodes' dictionary (ignoring, check config file): ", invalidNodes)
		}

		// Execute in sequence or in paralel as per command flag
		if paramBool["seq"] {
			execSequent(validNodes, user, nodes, cmdExec, bBlue)
		} else {
			execParallel(validNodes, user, nodes, cmdExec, bBlue)
		}

		break
	}
}
