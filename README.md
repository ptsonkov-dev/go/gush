# GuSH - execute command at multiple hosts 

GuSH is application created to execute same command to multiple nodes (list of 1 or more nodes) or group of nodes (list of 1 or more groups). 
- It is required to exchange SSH keys with target nodes before use. Login with password do not exists and won't be added.
- Application read predefined nodes/groups from configuration file and execute command in parallel (default) or sequential (optional) mode. As a result will get STDOUT/STDERR from each node. 
- Application do not support interactive mode, each command need to be clear, without to expect user input ultil it ends. 
- Each node or group listed in parameters but not existing in configuration file willl be ignored and listed as red alert.

## Build and install
- Clone repository
```
# mkdir /path/to/sources
# cd /path/to/sources
# git clone https://gitlab.com/ptsonkov-dev/go/gush.git
```
- Prerequisites - install needed packages
```
# go get github.com/fatih/color
# go get golang.org/x/crypto/ssh                      
# go get gopkg.in/yaml.v2
```
- Build
```
# cd /path/to/sources/gush
# go build
# mv gush /usr/local/bin
```

## Config file
- Valid filename is ___gush.yaml___
- File content. Below is sample file content, valid format is YAML. Please do not change the structure or application will fail
```
user: root
nodes:
    node1: 192.168.1.11:22
    node2: 192.168.1.12:22
    node3: 192.168.1.13:22
    node4: 192.168.1.14:22
    node5: 192.168.1.15:22
    node6: 192.168.1.16:22
    node7: 192.168.1.17:22
    node8: 192.168.1.18:22
groups:
    group1:
        - node1
        - node2
    group2:
        - node3
        - node4
        - node5
    group3:
        - node5
        - node6
```
- Valid configuration destinations. Application will search for configurations at following places, in below priority:
    - ~/gush.yaml
    - /etc/gush.yaml
    - ./gush.yaml (same directory as binary file)


## Usage
__NOTE:__ It is important to surround command in single quotes
- Get available inventory from configuration file
```
# gush -l
Work with congig file: ./gush.yaml
=== List configuration inventory (nodes and groups)

Nodes: 
node1,node2,node3,node4,node5,node6,node7,node8

Groups (and group members):
   group1 : 
     node1,node2
   group2 : 
     node3,node4,node5
   group3 : 
     node5,node6
```
- Execute against list of nodes:
```
# gush -n node1,node2,node3 'hostname && date && uptime'
Work with congig file: ./gush.yaml
=== Execute command against list of nodes (parallel, faster)
target nodes:  [node1 node2 node3]

node1 (root@192.168.1.11) => hostname && date && uptime
node1
Sun Sep 27 03:33:55 EEST 2020
 03:33:55 up 16 days,  7:15,  0 users,  load average: 0.00, 0.02, 0.06



node3 (root@192.168.1.13) => hostname && date && uptime
node3
Sun Sep 27 03:33:55 EEST 2020
 03:33:55 up 16 days,  7:15,  0 users,  load average: 0.16, 0.03, 0.01



node2 (root@192.168.1.12) => hostname && date && uptime
node2
Sun Sep 27 03:33:56 EEST 2020
 03:33:56 up 16 days,  7:15,  0 users,  load average: 0.03, 0.11, 0.09
```
- Execute against list of groups
```
$ ./gush -g group1,group2 'hostname && date && uptime'
Work with congig file: ./gush.yaml
=== Execute command against group of nodes (parallel, faster)
target groups:  [group1 group2]
affected nodes (group members):  [node1 node2 node5 node6]

node5 (root@192.168.1.15) => hostname && date && uptime
node5
Sun Sep 27 03:43:21 EEST 2020
 03:43:21 up 16 days,  7:25,  0 users,  load average: 0.00, 0.08, 0.09



node6 (root@192.168.1.16) => hostname && date && uptime
node6
Sun Sep 27 03:43:21 EEST 2020
 03:43:21 up 16 days,  7:25,  0 users,  load average: 0.01, 0.06, 0.08



node1 (root@192.168.1.11) => hostname && date && uptime
node1
Sun Sep 27 03:43:21 EEST 2020
 03:43:21 up 16 days,  6:07,  0 users,  load average: 0.02, 0.05, 0.02



node2 (root@192.168.1.12) => hostname && date && uptime
node2
Sun Sep 27 03:43:21 EEST 2020
 03:43:21 up 16 days,  7:25,  0 users,  load average: 0.00, 0.00, 0.00
```

## Available options
Full list of options and descriptions is available in application help
```
# gush -h
```
